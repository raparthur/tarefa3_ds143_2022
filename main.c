#include <stdio.h>
#include <stdlib.h>

    //================== DEF STRUCTS ==============
    typedef struct{
    int hora;
    int minuto;
    int segundo;
    }t_time;

    typedef struct{
    t_time chave;
    char* valor;
    struct t_timetable* prox;
    }t_timetable;

    //=========== PONTEIRO CABE�ALHO===============
    t_timetable* header = NULL;

    //===========MOSTRAR VALORES=========================
    void show_table()
    {
        t_timetable* aux = header;
        while(aux != NULL)
        {
            printf("%i:%i:%i => %s\n",aux->chave.hora,aux->chave.minuto,aux->chave.segundo,aux->valor);
            aux = aux->prox;
        }
    }
    //===================================================


    //############ COMPARA HORARIOS ############
    // Retorna:
    // 1  se h1 >  h2
    // 0  se h1 == h2
    // -1 se h1 <  h2

    int time_cmp(t_time h1, t_time h2){
        int segundos_h1 = 60*60*h1.hora + 60*h1.minuto + h1.segundo;
        int segundos_h2 = 60*60*h2.hora + 60*h2.minuto + h2.segundo;
        if(segundos_h1 > segundos_h2){
            return 1;
        }
        if(segundos_h1 < segundos_h2){
            return -1;
        }
        return 0;
    }

    //############################# remove chave e seu valor da tabela ########################################################
    void delete(t_time key){
        t_timetable *atual, *anterior, *proximo;

        atual = header;
        anterior = NULL;
        proximo = NULL;

        if(atual != NULL){
            if(atual->prox == NULL){
                free(atual);
            } else {
                while(atual->prox != NULL){
                    anterior = atual;
                    atual = atual->prox;
                    proximo = atual->prox;
                    if(time_cmp(atual->chave,key) == 0){
                        free(atual);
                        anterior->prox = proximo;
                        break;
                    }
                }
            }

        }

    }

    //###### insere par key-value na tabela (remove elemento se valor for null) Extra: J� otimizado para ordem crescente ######
    void put(t_time key, char * val){

    if(val == NULL){
        delete(key);
        return;
    }
    t_timetable *atual, *novo, *anterior;

    novo = (t_timetable *) malloc(sizeof(t_timetable));

    atual = header;
    anterior = NULL;

    novo->valor = val;
    novo->chave = key;

    if(atual == NULL){
        novo->prox = NULL;
        header = novo;
    } else{
        while(atual != NULL && time_cmp(key,atual->chave) >= 0){

            anterior = atual;
            atual = atual->prox;
        }

        novo->prox = atual;

        if(anterior == NULL){
                header = novo;
            } else{
                anterior->prox = novo;
            }
        }
    }
    //######### valor armazenado com a chave key (null se key n�o existe) ####################
    char* get(t_time key){
        t_timetable* aux = header;
        while(aux != NULL){
            if(time_cmp(key,aux->chave) == 0){
                return aux->valor;
            }
            aux = aux->prox;
        }
        return NULL;
    }

    //######existe um valor com a chave key?################################################
    _Bool contains(t_time key){
        return get(key) != NULL;
    }

    //######a tabela est� vazia?################################################
    _Bool is_empty(){
        return header == NULL;
    }
    //######n�mero de pares key-value armazenados na tabela#################################
    int size(){
        t_timetable* aux = header;
        int count = 0;
        while(aux != NULL)
        {
            count++;
            aux = aux->prox;
        }
        return count;
    }
    //###### menor chave armazenada ################################
    t_time min(){
        if(!is_empty()){
            return header->chave; //a tabela j� est� ordenada crescente
        }
    }

    //###### maior chave armazenada ################################
    t_time max(){
       if(!is_empty()){
            t_timetable* aux = header;
            //busca o �ltimo, pois j� est� em ordem crescente
            while(aux->prox != NULL)
            {
                aux = aux->prox;
            }
            return aux->chave;
        }
    }


    //###### maior chave armazenada menor ou igual � key ################################
    t_time floor(t_time key){

        t_timetable* aux = header;
        while(aux != NULL)
        {
            if(time_cmp(key,aux->chave) >= 0){
                if(aux->prox == NULL){
                    return aux->chave;
                }
                t_timetable* proximo = aux->prox;
                if(time_cmp(key,proximo->chave) < 0){

                    return aux->chave;
                }

            }

            aux = aux->prox;
        }
    }

    //###### menor chave armazenada maior ou igual � key ################################
    t_time ceiling(t_time key){
        t_timetable* aux = header;
        while(aux != NULL)
        {
            //j� est� em ordem crescente, ent�o retorna o primeiro maior ou igual
            if(time_cmp(key,aux->chave) <= 0){
                return aux->chave;
            }

            aux = aux->prox;
        }
    }

    //###### n�mero de chaves armazenadas menores do que key ################################
    int rank(t_time key){
        t_timetable* aux = header;
        int count = 0;
        while(aux != NULL)
        {
            //conta se a chave for maior
            if(time_cmp(key,aux->chave) > 0){
                count++;
            }

            aux = aux->prox;
        }
        return count;
    }

    //###### chave de rank igual a k ################################
    t_time select(int k){
        t_timetable* aux = header;
        while(aux != NULL)
        {
            if(rank(aux->chave) == k){
                return aux->chave;
            }
            aux = aux->prox;
        }
    }

    //###### remove a menor chave ################################
    void delete_min(){
        t_timetable* aux = header;
        if(aux != NULL)
        {
            header = aux->prox;
            free(aux);
        }
    }

    //###### remove a maior chave ################################
    void delete_max(){
        t_timetable* aux = header;
        if(aux != NULL)
        {
            while(aux != NULL)
            {
                if(aux->prox == NULL){
                    delete(aux->chave);
                }
                aux = aux->prox;
            }
        }
    }

int main()
{

    t_timetable t1,t2,t3,t4,t5;

    char* pChar1 = (char*) calloc(10,sizeof(char));
    char* pChar2 = (char*) calloc(10,sizeof(char));
    char* pChar3 = (char*) calloc(10,sizeof(char));
    char* pChar4 = (char*) calloc(10,sizeof(char));
    char* pChar5 = (char*) calloc(10,sizeof(char));

    pChar1 = "key 1";
    pChar2 = "key 2";
    pChar3 = "key 3";
    pChar4 = "key 4";
    pChar5 = "key 5";

    t_time time1;

    time1.hora = 12;
    time1.minuto = 10;
    time1.segundo = 28;

    t_time time2;

    time2.hora = 12;
    time2.minuto = 10;
    time2.segundo = 18;

    t_time time3;

    time3.hora = 2;
    time3.minuto = 49;
    time3.segundo = 1;

    t_time time4;

    time4.hora = 6;
    time4.minuto = 32;
    time4.segundo = 16;

    t_time time5;

    time5.hora = 11;
    time5.minuto = 23;
    time5.segundo = 54;

    put(time1,pChar1);
    put(time2,pChar2);
    put(time3,pChar3);
    put(time4,pChar4);
    put(time5,pChar5);

    show_table();

    printf("--------deletado key 2 e 4------------\n");

    put(time2,NULL);
    delete(time4);
    show_table();

    printf("--------inserido key 2 ------------\n");

    put(time2,pChar2);
    show_table();

    printf("--------get key 3 ------------\n");

    printf("valor: %s\n",(get(time3)));

    printf("--------imprimindo ------------\n");
    show_table();

    printf("--------floor de 10:0:0 ------------\n");
    t_time timef;
    timef.hora = 10;
    timef.minuto = 0;
    timef.segundo = 0;
    timef = floor(timef);
    printf("RESULT: %i:%i:%i\n",timef.hora,timef.minuto,timef.segundo);

    printf("--------ceiling de 10:0:0 ------------\n");
    t_time timec;
    timec.hora = 10;
    timec.minuto = 0;
    timec.segundo = 0;
    timec = ceiling(timec);
    printf("RESULT: %i:%i:%i\n",timec.hora,timec.minuto,timec.segundo);

    printf("--------rank de 10:0:0 ------------\n");
    t_time timer;
    timer.hora = 10;
    timer.minuto = 0;
    timer.segundo = 0;
    printf("RESULT: %i\n",rank(timer));

    printf("--------chave de rank = 2 ------------\n");
    t_time timek = select(2);
    printf("RESULT: %i:%i:%i\n",timek.hora,timek.minuto,timek.segundo);

    printf("------- deletado extremos ----------\n");
    delete_min();
    delete_max();
    show_table();

    printf("--------inserido key 1, 3,  4 ------------\n");
    put(time1,pChar1);
    put(time3,pChar3);
    put(time4,pChar4);
    show_table();







    return 0;
}
